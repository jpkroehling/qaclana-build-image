FROM golang:1.10

ADD protoc/ /go/
ADD cockroach/cockroach /go/bin/
ADD test-reporter-latest-linux-amd64 /bin/cc-test-reporter
RUN /usr/local/go/bin/go get -u github.com/golang/protobuf/protoc-gen-go
RUN /usr/local/go/bin/go get -u golang.org/x/vgo
RUN /usr/local/go/bin/go get -u github.com/golang/lint/golint
RUN /usr/local/go/bin/go get -u github.com/axw/gocov/gocov
RUN /usr/local/go/bin/go get github.com/GoASTScanner/gas/cmd/gas/...
